$(function () {
    $('.nav-toggle').click(function(e){
        e.preventDefault();
        $('.nav-toggle').toggleClass('active');
        $('.nav-menu').toggleClass('active');
        $('.nav-overlay').toggleClass('active');
    });
    /** banner */
    $('.fs-carousel').flickity({
        cellAlign: 'left',
      setGallerySize: false,
        contain: true,
      prevNextButtons: false,
      pageDots: false,
      wrapAround: true,
      autoPlay: 5000,
      percentPosition: false,
    });
    /** END banner */

    
});