$(document).ready(function() {
    $('body').imagesLoaded({
        background: true
    }, function() {
        TweenMax.to('.loading .logo', 0.6, {
        alpha: 0,
        scale: 1.5,
        delay: 0.5,
        onComplete: function() {
            //APP.init();
            TweenMax.to('.loading', 0.6, {
            alpha: 0,
            onComplete: function() {
                $('body').removeClass('loader');
                $('.loading').remove();
            }
            });
        }
        });
    });
});
//const APP = {};